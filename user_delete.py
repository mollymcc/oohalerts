from os import name
import yaml
import datetime
import time
from variables import *


def formatted_entry(entry):

    return f"""Name: {entry['FirstName']} {entry['LastName']}\nEmail: {entry['Email']}\nDescription: {entry['Description']}\nDate: {entry["Date"]}\nTime: {entry['Time']}\nID: {entry['ID']}\n"""


def update_yaml(data):

    with open(yml_filename, "w") as yaml_file_update:
        yaml.dump(data, yaml_file_update, default_flow_style=False)


def delete():
    try:


        with open(yml_filename, "r") as yaml_file:
            data = yaml.load(yaml_file, Loader=yaml.FullLoader)


        ## splits by space and joins the name for comparison later
        name = input("What is your name: ").split(" ")
        name = "".join(name)


        ## List comprehend the below
        ## Something like this
        # dic = [entry for entry in [entries for entries in data[entries]] if name.lower() in (entry['FirstName'] + entry["LastName"]).lower()]
        list_by_name = []
        for entries in data:
            for entry in data[entries]:
                ## Compares the joined first/last inputted name with the joined first/last name from the yaml file
                if name.lower() in (entry['FirstName'] + entry["LastName"]).lower():
                    ## "entries" is the date which is the key of the data dictionary
                    entry["Date"] = entries
                    list_by_name.append(entry)

        for id,entry in enumerate(list_by_name):
            list_by_name[id]["ID"] = id
            print(formatted_entry(entry))


        ## Could potentially add the date input to filter further
        # date = input("What date is your task: ")


        id = int(input("What is the ID of the entry you'd like to delete: "))

        for entry in list_by_name:
            if entry["ID"] == id:
                option = input(f"\n{formatted_entry(entry)}\n\nWould you like to delete this entry? (y/n)")
                date = entry["Date"]

        if option.lower()[0] == "y":
            for entry in data[date]:
                if entry["ID"] == id:
                    list_index = data[date].index(entry)
                    del data[date][list_index]

            update_yaml(data)

        elif option.lower()[0] == "n":
            print("ok u indecisve person")

        else:
            print("pick a valid option")
    
    except UnboundLocalError:
        print("Sorry, your input wasn't valid, try again \n")
        delete()
    except KeyboardInterrupt:
        print("\nCtrl-C was Entered \n")
        time.sleep(0.3) 
        print("Exiting the program \n")
    except ValueError:
        print("Invalid entry, try again please. \n")
        delete()

if __name__ == "__main__":
    delete()
