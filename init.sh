#!/bin/bash

## Variables
export HOST_IP="54.246.253.187"
export PATH_TO_SSH_KEY="~/.ssh/sirs.pem"

## Running Fabric_cp to send fresh files
python3 fabric_cp.py

## copy init script into machine
# scp -o StrictHostKeyChecking=no -i $PATH_TO_SSH_KEY $INIT_SCRIPT_PATH ec2-user@$HOST_IP:/$DEST_FILE_PATH


ssh -tt -o StrictHostKeyChecking=no  -i $PATH_TO_SSH_KEY ec2-user@$HOST_IP '
# sudo chmod u+x /home/ec2-user/init.sh
# ./home/ec2-user/init.sh


##################################
# DO DEPENDENCY INSTALLATIONS HERE
##################################

export CONTAINER_NAME="ooh_alert_container"
export IMAGE_NAME="ooh_alert_image"
export DEST_FILE_PATH="/home/ec2-user"


## check if container exists
echo "Checking if OOH Alert Container is running..."
if docker ps | grep $CONTAINER_NAME
then
    echo "OOH Alert Container found"
else
    echo "OOH Alert Container not found, updating image and creating new container... "
    cd $DEST_FILE_PATH
    docker build -t $IMAGE_NAME .
    docker run -dit --name $CONTAINER_NAME $IMAGE_NAME
fi

'