#!/bin/bash
# script to ask user whether they want to append, delete, update or read an OOH task
hostname_1='54.246.253.187'
entry_word_num=0
echo ""
echo 'What function would you like to carry out?'
echo ""
echo '1. Delete an entry'
echo '2. Update an entry'
echo '3. Append a new entry'
echo '4. Read the OOH tasks'
echo ""
read -p 'Please enter your corresponding number: ' entry_number
if [ $entry_number == 1 ] ; then
echo 'You chose to delete an entry
'
sleep 0.5
ssh -tt -o StrictHostKeyChecking=no  -i ~/.ssh/oohkey.pem ec2-user@$hostname_1 '
sudo docker exec -it oohalertcontainer python3 user_delete.py
'

elif [ $entry_number == 2 ] ; then
echo '
You chose to update an exsisting entry
'

sleep 0.5
ssh -tt -o StrictHostKeyChecking=no  -i ~/.ssh/oohkey.pem ec2-user@$hostname_1 '
sudo docker exec -it oohalertcontainer python3 user_edit.py
'


elif [ $entry_number == 3 ] ; then
echo '
You chose to append a new OOH task entry
' 
sleep 0.5
ssh -tt -o StrictHostKeyChecking=no  -i ~/.ssh/oohkey.pem ec2-user@$hostname_1 '
sudo docker exec -it oohalertcontainer python3 user_append.py
'

elif [ $entry_number == 4 ] ; then
echo 'You chose to read all the OOH entries
' 
sleep 0.5
ssh -tt -o StrictHostKeyChecking=no  -i ~/.ssh/oohkey.pem ec2-user@$hostname_1 '
sudo docker exec -it oohalertcontainer cat user.yaml
'
else
entry_word_num=1
echo ""
read -p "You didn't enter a correct number/entry, would you like to try running the program again?

Enter y or n: "  entry_word
if [ $entry_word == 'y' ] ; then
./user_use_app.sh
else
echo '
Exiting the program'
#echo 'no'
fi
fi

#echo $entry_word_num
if [ $entry_word_num == 0 ] ; then
read -p "
Would you like to run the program again? 

Enter y or n: " run_again
if [ $run_again == 'y' ] ; then
./user_use_app.sh
else
echo '
Exiting the program'
fi
fi