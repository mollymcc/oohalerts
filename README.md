# OOH Notifier README #

- [OOH Notifier README](#ooh-notifier-readme)
  - [Introduction](#introduction)
  - [Setting Up and Initialising Docker Container (`init.sh`)](#setting-up-and-initialising-docker-container-initsh)
      - [Farbic](#farbic)
      - [Dockerfile](#dockerfile)
      - [Cronjob](#cronjob)
  - [Scripts](#scripts)
    - [Appending an entry to the YAML (`user_append.py`)](#appending-an-entry-to-the-yaml-user_appendpy)
    - [Deleting an entry to the YAML (`user_delete.py`)](#deleting-an-entry-to-the-yaml-user_deletepy)
    - [Reading an entry from the YAML (`user_read.py`)](#reading-an-entry-from-the-yaml-user_readpy)
    - [Editing an entry to the YAML (`user_edit.py`)](#editing-an-entry-to-the-yaml-user_editpy)
    - [Notifying users of entries (`slack_notify.py`)](#notifying-users-of-entries-slack_notifypy)
  - [Using the App (`user_use_app.sh`)](#using-the-app-user_use_appsh)


## Introduction
This repository contains the code for the HMPO task to deal with alerting for scheduled out-of-hours work.

All Out-Of-Hours work will be read from a file by a docker container, which then triggers an alert via a Slack API call from a python script.

## Setting Up and Initialising Docker Container (`init.sh`)
These scripts are setup to run in a docker container. The container is provisioned with the Dockerfile and started by the init.sh script.


- The first section of the init.sh script sets the variables for certain parameters.
```bash
export HOST_IP="54.246.253.187"  ## The IP of the EC2 instance the Docker container is running in
export PATH_TO_SSH_KEY="~/.ssh/sirs.pem" ## Path to the SSH Key which allows SSH access to the EC2 Instance
export DEST_FILE_PATH="/home/ec2-user" ## The destination file path in the instance where the scripts would be copied into
```
#### Farbic
- The init script then copies over the Python scripts through the `fabric_cp.py` script.

  > This copies up all the cronjob and Dockerfile files and all the txt, python and yaml files needed for OOH alerts program.

#### Dockerfile
- The init script then SSH's into the EC2 instance and runs a set of commands which provisions the machine and creates a docker container from the Dockerfile if none exist with the docker container name already.

- The Dockerfile installs all the dependencies that are needed for the OOH notifier from the requirements.txt file.
-  The Dockerfile also ensures that the correct Timezone is setup for the container, as otherwise the container runs an hour ahead of UK time. 
#### Cronjob
- The init script also installs `cron` which is used to set up a cronjob which runs the slack_notify.py script every single minute. This is done using the notation `* * * * *` . 
- If you wish to alter the frequency in which the program runs the slack_notify.py script you would simply change this notation. For exmaple, `*/2 * * * *` would set to run every second minute. 
  
## Scripts

### Appending an entry to the YAML (`user_append.py`)


### Deleting an entry to the YAML (`user_delete.py`)


### Reading an entry from the YAML (`user_read.py`)


### Editing an entry to the YAML (`user_edit.py`)

### Notifying users of entries (`slack_notify.py`)

### Cronjob which reguarly checks YAML file to notify 



## Using the App (`user_use_app.sh`)
- For a user to actually use the app to use any of the functionality, they must run the `user_use_app.sh` script. Using the command

```bash
./user_use_app.sh 
```

-  This user runs this script locally. This provides an intuitive prompts which the user can answer depending on the functionality they want to use.
  - For example, once having run the script, if the user would like to append an entry, they would pick option 3.
  
![User_use_picture](pictures/user_use_picture.png)

  - The script would then simply ssh into the EC2 instance and run the `user_append.sh` script
  