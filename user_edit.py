from os import name
import yaml
from variables import *
import user_append


def formatted_entry(entry):

    return f"""Name: {entry['FirstName']} {entry['LastName']}\nEmail: {entry['Email']}\nDescription: {entry['Description']}\nDate: {entry["Date"]}\nTime: {entry['Time']}\nID: {entry['ID']}\n"""


def update_yaml(data):

    with open(yml_filename, "w") as yaml_file_update:
        yaml.dump(data, yaml_file_update, default_flow_style=False)


def edit():


    with open(yml_filename, "r") as yaml_file:
        data = yaml.load(yaml_file, Loader=yaml.FullLoader)


    ## splits by space and joins the name for comparison later
    name = input("What is your name: ").split(" ")
    name = "".join(name)


    ## List comprehend the below
    ## Something like this
    # dic = [entry for entry in [entries for entries in data[entries]] if name.lower() in (entry['FirstName'] + entry["LastName"]).lower()]
    list_by_name = []
    for entries in data:
        for entry in data[entries]:
            ## Compares the joined first/last inputted name with the joined first/last name from the yaml file
            if name.lower() in (entry['FirstName'] + entry["LastName"]).lower():
                ## "entries" is the date which is the key of the data dictionary
                entry["Date"] = entries
                list_by_name.append(entry)

    for id,entry in enumerate(list_by_name):
        list_by_name[id]["ID"] = id
        print(formatted_entry(entry))


    ## Could potentially add the date input to filter further
    # date = input("What date is your task: ")


    id = int(input("What is the ID of the entry you'd like to edit: "))

    for entry in list_by_name:
        if entry["ID"] == id:
            option = input(f"\n{formatted_entry(entry)}\nWould you like to edit this entry? (y/n)\n")
            ## We need to store the date for filtering later 
            date = entry["Date"]
            entry_to_edit = entry

    del entry_to_edit["ID"]

    for key, field in enumerate(entry_to_edit):
        print(f"({key+1}) {field}: {entry_to_edit[field]}")
    
    field_to_edit_key = input("\nWhat field would you like to change? Enter field name or number:\n")

    for key, field in enumerate(entry_to_edit):
        if int(field_to_edit_key) == key+1:
            print(f"Current {field}: {entry_to_edit[field]}")
            ## Changing the entry of the specific field
            changed_field = input(f"New {field}: " )

            for entry in data[date]:
                if entry == entry_to_edit:
            #         ## We store the index to delete the extra "date" field later
                    entry_index = data[date].index(entry)
                    if field != "Date":
                        data[date][entry_index][field] = changed_field
                        update_yaml(data)
                    else:
                        date_for_job=changed_field
                        firstname=entry['FirstName']
                        lastname=entry['LastName']
                        email=entry['Email']
                        description=entry['Description']
                        time=entry['Time']
                        to_add = {
                            'FirstName': firstname,
                            'LastName': lastname,
                            'Email': email,
                            'Description': description,
                            'Time': time
                        }

    ## If the date already exists as a key in the dictionary then it appends to the existing key (date)
                        if date_for_job in data:
                            data[date_for_job].append(to_add)
        
    ## Else it creates a new key of the date
                        else:
                            data[date_for_job]=[to_add]
    del data[date][entry_index]["Date"]              
    if field_to_edit_key == 'Date' or field_to_edit_key == '6':
        del data[date][entry_index]
        print(f"""\nThe updated entry is now: \n\nName: {entry['FirstName']} {entry['LastName']}\nEmail: {entry['Email']}\nDescription: {entry['Description']}\nDate: {changed_field}\nTime: {entry['Time']}\n""")
        
    else:
        print(f"""\nThe updated entry is now: \n\nName: {entry['FirstName']} {entry['LastName']}\nEmail: {entry['Email']}\nDescription: {entry['Description']}\nDate: {date}\nTime: {entry['Time']}\n""")
    if data[date] == []:
        del data[date]
    update_yaml(data)


if __name__ == "__main__":
    edit()