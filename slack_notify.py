import requests
import user_read
from variables import *


def notify():
    message = ""

    for alert in user_read.datetime_compare():
        message += alert + "\n\n"

    if message == "":
        pass
    else:
        payload = """
        {
        "text": """ '"' + message + '"' """
        }
        """

        r = requests.post(SLACK_URL, data=payload)
        print(r.status_code)

if __name__ == "__main__":
    notify()


########################################################################

