import yaml
import datetime
from variables import *

with open(yml_filename, "r") as yaml_file:
    f = yaml.load(yaml_file, Loader=yaml.FullLoader)

def formatted_entry(entry, slack_id, timedate_combined):
    return f"{entry['FirstName']} {entry['LastName']} {slack_id}\n Description: {entry['Description']} \n Date and Time: {timedate_combined} "


def datetime_compare():

    for date in f:

        for entry in f[date]:

            time = entry["Time"]
            timedate_combined = datetime.datetime.strptime(date + " " + time, "%d/%m/%Y %H:%M")
            delta = timedate_combined - now

            if entry['FirstName'] in dict1:
                slack_id = dict1[entry['FirstName']]
            else:
                slack_id = ''

            ## This window of time needs to be the same as the crontab interval of time, currently set to one minute
            ## To ensure no notifications get missed
            if delta <= time_upper_bound and delta > time_lower_bound:
                
                string = formatted_entry(entry, slack_id, timedate_combined)
                yield string

def show_alerts():
    for alert in datetime_compare():
        print(alert)

if __name__ == "__main__":
    show_alerts()
