from tkinter import *
import user_append

window = Tk()
window.geometry('720x450')
window.title("OOH form")
Title_Label = Label(window, text="Out Of Hours Form", font="helvetica 16").grid(column =0, columnspan =2, pady=(10,5), padx = (50,0))
Subtitle_Label = Label(window, text="Please fill out each section of this form to ensure your OOH reminder can be scheduled correctly", font="helvetica 13").grid(column =0, columnspan =2, pady=(10,50), padx=(50,0))
First_Name_Label = Label(window, text="First Name").grid(row=2, pady=4, padx=10)
Second_Name_Label = Label(window, text="Last Name").grid(row=3, pady=4, padx=10)
Email_Address_Label = Label(window, text="Email Address").grid(row=4, pady=4, padx=10)
OOH_desc_label = Label(window, text="OOH reminder description").grid(row=5, pady=4, padx=10)
date_for_job_Label = Label(window, text="Date of job (DD/MM/YYYY)").grid(row=6, pady=4, padx=10)
time_for_job_label = Label(window, text="Time of job (HH:MM)").grid(row=7, padx=10, pady=(10))
First_Name_1 = Entry(window)
First_Name_1.config(width=40)
Second_Name_1 = Entry(window)
Second_Name_1.config(width=40)
Email_Address_1 = Entry(window)
Email_Address_1.config(width=40)
OOH_desc_1 = Entry(window)
OOH_desc_1.config(width=40)
Date_job_1 = Entry(window)
Date_job_1.config(width=40)
Time_job_1 = Entry(window)
Time_job_1.config(width=40)


First_Name_1.grid(row=2, column=1, pady =4, padx=10)
Second_Name_1.grid(row=3, column=1, pady =4, padx=10)
Email_Address_1.grid(row=4, column=1, pady =4, padx=10)
OOH_desc_1.grid(row=5, column=1, pady =4, padx=10)
Date_job_1.grid(row=6, column=1, pady =4, padx=10)
Time_job_1.grid(row=7, column=1, pady =4, padx=10)


def getInput():

    First_Name = First_Name_1.get()
    Second_Name = Second_Name_1.get()
    Email_Address = Email_Address_1.get()
    OOH_desc = OOH_desc_1.get()
    Date_job = Date_job_1.get()
    Time_job = Time_job_1.get()
    window.destroy()

    global params
    params = [First_Name, Second_Name, Email_Address, OOH_desc, Date_job, Time_job]
    #return params

Button(window, text = "submit",
           command = getInput).grid(row = 8, column=1, sticky = W, padx=60, pady=(40,0))
mainloop()

print(type(params[4]))
user_append.append_to_users(firstname = params[0], lastname=params[1], email=params[2], description=params[3], date_for_job=str(params[4]), time=params[5])