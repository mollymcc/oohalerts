import datetime

SLACK_URL = "https://hooks.slack.com/services/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"
yml_filename = "user.yaml"
# now = datetime.datetime(2021, 8, 23, 12, 31, 0, 0)
now = datetime.datetime.now()
dict1 = {'Ramana': '<@U01T2KUC951>', 'Selina': '<@U01SVL3U23Y>', 'Steph':'<@U01SZB97DL5>'}

## The difference in this upper bound should be equal to the interval of time the cronjob is run
## Currently the cronjob runs every minute and so to be reminded 10 minutes before a job the upper bound must be set to ten minutes and the lower bound must be set to 1 minute less than the upper bound.
time_upper_bound = datetime.timedelta(minutes=10)
time_lower_bound = datetime.timedelta(minutes=9)