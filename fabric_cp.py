from fabric.api import env
from fabric.operations import run, put

host_ip = '54.246.253.187'
path_to_pem = '~/.ssh/sirs.pem'

## Include the last / in the file path please
source_file_path = '/Users/ramanaarulljothiechanthira/code/hmpo/hmpo_ooh/'
dest_file_path = '/home/ec2-user/'
ext_to_copy = ['*.txt', '*.yaml','cronjob','Dockerfile', '*.py']
 
env.host_string = 'ec2-user@'+host_ip
env.key_filename = path_to_pem
def copy():   

    run(f'mkdir -p {dest_file_path}')

    for extension in ext_to_copy:
        put(source_file_path+extension, dest_file_path)
    
copy()