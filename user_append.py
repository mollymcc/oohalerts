import yaml
from variables import *


try:
    with open(yml_filename, "r") as yaml_file:
        data = yaml.load(yaml_file, Loader=yaml.FullLoader)
        if data is None:
            data = {}
## If yaml file doesn't exist, it creates it
except FileNotFoundError as e:
    with open(yml_filename, "w") as yaml_file:
        data = {}
except Exception as e:
    print(e)


## This function takes Key, Value pairs from a dictionary and format that into a sentence
def formatted_entry(entry, date):

    return f"""Name: {entry['FirstName']} {entry['LastName']}\nEmail: {entry['Email']}\nDescription: {entry['Description']}\nDate: {date}\nTime: {entry['Time']}\n"""



def append_to_users(date_for_job, firstname, lastname, email, description, time):

    to_add = {
        'FirstName': firstname,
        'LastName': lastname,
        'Email': email,
        'Description': description,
        'Time': time
    }

    ## If the date already exists as a key in the dictionary then it appends to the existing key (date)
    if date_for_job in data:
        data[date_for_job].append(to_add)

    ## Else it creates a new key of the date
    else:
        data[date_for_job]=[to_add]
    
    print(f"\nThe following entry has been added:\n{formatted_entry(entry = to_add, date = date_for_job)}\n")

    ## Updates the yml file with the new entry
    with open(yml_filename, "w") as yaml_file_update:
        yaml.dump(data, yaml_file_update, default_flow_style=False)


def ask_details():

    ## Simple user prompt 
    date_for_job = input("What is the date of the job (DD/MM/YYYY): ")
    firstname = input("What is your first name: ")
    lastname = input("What is your last name: ")
    email = input("What is your email: ")
    description =  input("What is the task: ")
    time = input("What time is the job (HH:MM): ")

    append_to_users(date_for_job=date_for_job, firstname=firstname, lastname=lastname, email=email, description=description, time=time)


if __name__ == "__main__":
    ask_details()