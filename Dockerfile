FROM ubuntu:latest

RUN mkdir /oohalert

WORKDIR /oohalert
COPY *.py ./
COPY *.txt ./
COPY *.yaml ./

RUN apt-get update && apt-get -y install python3-pip

RUN pip3 install -r requirements.txt

RUN apt-get -y install cron

# Copy hello-cron file to the cron.d directory
COPY cronjob /etc/cron.d/cronjob

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/cronjob

# Apply cron job
RUN crontab /etc/cron.d/cronjob
 
# Create the log file to be able to run tail
RUN touch /var/log/cron.log
 
# Run the command on container startup
CMD cron && tail -f /var/log/cron.log

# Timezone env with default
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get install -y tzdata

RUN dpkg-reconfigure --frontend noninteractive tzdata

